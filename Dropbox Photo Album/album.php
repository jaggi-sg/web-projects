<!-- 
Name: JAGADISH SHIVANNA
ID: 1001050680
URL: http://omega.uta.edu/~jxs0680/project8/album.php 
Reference: http://www.w3schools.com/php/php_file_upload.asp (Validation of image)-->
<html>
<head><title>Dropbox Photo Album</title></head>
<style>
#btn {
	background-color: #1A5C40;
	-webkit-border-radius: 5px;
	color: #fff;
	font-family: 'Palatino Linotype';
	font-size: 16px;
	border: solid 1px #42AED8;
} 
#file {
	background-color: #B1D9F2;
	font-size: 16px;
	border: solid 1px #42AED8;
}
#btn1 {
	background-color: #E7F78D;
	color: black;
	font-family: 'Arial';
	border: solid 1px #42AED8;
	font-size: 16px;
} 
span, th, td {
	font-family: 'Palatino Linotype';
	font-size: 16px;
}
</style>
<body bgcolor="#98F5BF">
<center><h1>DROPBOX Photo Album</h1></center><hr/>
<!-- Form to upload image -->
<form enctype="multipart/form-data" method="POST" action="album.php">
<p>
<span><label for="file">Upload Image:</label></span>
<input type="file" name="fileUpload" id="file"/>
</p>
<p><input type="submit" name="submit" id = "btn" value="Upload!"></p>
</form>
<?php
// display all errors on the browser
error_reporting(E_ALL);
/* ini_set('display_errors','On');
 */
?>
<?php
/** 
 * DropPHP sample
 *
 * http://fabi.me/en/php-projects/dropphp-dropbox-api-client/
 *
 * @author     Fabian Schlieper <fabian@fabi.me>
 * @copyright  Fabian Schlieper 2012
 * @version    1.1
 * @license    See license.txt
 *
 */

// these 2 lines are just to enable error reporting and disable output buffering (don't include this in you application!)
error_reporting ( E_ALL );
enable_implicit_flush ();
// -- end of unneeded stuff

// if there are many files in your Dropbox it can take some time, so disable the max. execution time
set_time_limit ( 0 );

require_once ("DropboxClient.php");

// you have to create an app at https://www.dropbox.com/developers/apps and enter details below:
$dropbox = new DropboxClient ( array (
		'app_key' => "cp0jrlliekuoctl", // Put your Dropbox API key here
		'app_secret' => "m2z7neucs8f0umd", // Put your Dropbox API secret here
		'app_full_access' => false 
), 'en' );

// first try to load existing access token
$access_token = load_token ( "access" );
if (! empty ( $access_token )) {
	$dropbox->SetAccessToken ( $access_token );
	/* echo "loaded access token:";
	print_r ( $access_token ); */
} elseif (! empty ( $_GET ['auth_callback'] )) // are we coming from dropbox's auth page?
{
	// then load our previosly created request token
	$request_token = load_token ( $_GET ['oauth_token'] );
	if (empty ( $request_token ))
		die ( 'Request token not found!' );
		
		// get & store access token, the request token is not needed anymore
	$access_token = $dropbox->GetAccessToken ( $request_token );
	store_token ( $access_token, "access" );
	delete_token ( $_GET ['oauth_token'] );
}

// checks if access token is required
if (! $dropbox->IsAuthorized ()) {
	// redirect user to dropbox auth page
	$return_url = "http://" . $_SERVER ['HTTP_HOST'] . $_SERVER ['SCRIPT_NAME'] . "?auth_callback=1";
	$auth_url = $dropbox->BuildAuthorizeUrl ( $return_url );
	$request_token = $dropbox->GetRequestToken ();
	store_token ( $request_token, $request_token ['t'] );
	die ( "Authentication required. <a href='$auth_url'>Click here.</a>" );
}

echo "<pre>";
/* echo "<b>Account:</b>\r\n";
print_r($dropbox);
print_r ( $dropbox->GetAccountInfo () ); */

// Upload Logic 
if(isset($_POST["submit"])) {
$uploadImg = $_FILES["fileUpload"]["name"];
$checkIfImage = getimagesize($_FILES["fileUpload"]["tmp_name"]);
echo "<pre>";
if($checkIfImage != false) {
$uploadSuccess = $dropbox->UploadFile ($_FILES["fileUpload"]["tmp_name"], $uploadImg);
/* print_r($uploadSuccess); */
echo "<span>Uploaded a ".$checkIfImage["mime"]." file:&nbsp;<b>$uploadImg</b></span>";
}
else
echo "<span>File is not an image!<br>Upload image files only!!!!</span>";
echo "</pre>";
}
// End upload logic

// Start Delete Logic
if(isset($_POST["delete"])) {
$del = $_POST["delete"];
/* print_r($del); */
$imgToBeDeleted = key($del);
echo $imgToBeDeleted . " <span>deleted</span>";
$deleteFile = $dropbox->Delete ($imgToBeDeleted);
}
//End Delete Logic

$files = $dropbox->GetFiles ( "", false );

if (empty ( $files )) {
	$dropbox->UploadFile ( "leonidas.jpg" );
	$dropbox->UploadFile ( "laptop1.jpg" );	
	$files = $dropbox->GetFiles ( "", false );
}

echo "\r\n\r\n<span><b>Images in the Dropbox directory:</b></span>\r\n";
/* print_r ( ( $files ) ); */

//Start Display window
?>
<form action="album.php" method="POST">
<table border="1">
<tr><th>View Image</th><th>Images in the Directory</th><th>Delete?</th></tr>
<?php foreach($files as $value) { ?>
<tr>
<td>
<center><input type="button" onclick="onClickChangeImg('<?= $dropbox->GetLink ( $value, false ) ?>')" name="open" value="open" id="btn1" /></center>
</td>
<td>
<?php $thumb = base64_encode ( $dropbox->GetThumbnail ( $value->path ) ); ?>
<img src="data:image/jpeg;base64,<?= $thumb ?>" /><span>&nbsp;&nbsp;<?php echo $value->path ?></span>
</td>
<td><input type="submit" name="delete[<?= $value->path ?>]" value="delete" id="btn1"/></td>
</tr>
<?php } ?>
</table>
</form>
<?php
// End Display window

// Start Display Image Section
?>
<script type="text/javascript">
function onClickChangeImg(image){
  document.getElementById("imageHolder").src = image;
}
</script>
<?php ?>
<div id="imageDiv"><span>Image Section: </span>
<img id="imageHolder" />
</div>
<?php
// End Display Image Section

if (! empty ( $files )) {
	/* $file = reset ( $files );
	$test_file = "test_download_" . basename ( $file->path );
	
	foreach($files as $value) {
		echo "<img src='" . $dropbox->GetLink ( $value, false ) . "'/></br>";
		echo "\r\n\r\n<b>Meta data of <a href='" . $dropbox->GetLink ( $value ) . "'>$value->path</a>:</b>\r\n";
		echo "\r\n\r\n<b>Downloading $value->path:</b>\r\n";
		$img_data = base64_encode ( $dropbox->GetThumbnail ( $value->path ) );
		echo "<img src=\"data:image/jpeg;base64,$img_data\" alt=\"Generating PDF thumbnail failed!\" style=\"border: 1px solid black;\" />";	
				} */
}

function store_token($token, $name) {
	if (! file_put_contents ( "tokens/$name.token", serialize ( $token ) ))
		die ( '<br />Could not store token! <b>Make sure that the directory `tokens` exists and is writable!</b>' );
}
function load_token($name) {
	if (! file_exists ( "tokens/$name.token" ))
		return null;
	return @unserialize ( @file_get_contents ( "tokens/$name.token" ) );
}
function delete_token($name) {
	@unlink ( "tokens/$name.token" );
}
function enable_implicit_flush() {
	@apache_setenv ( 'no-gzip', 1 );
	@ini_set ( 'zlib.output_compression', 0 );
	@ini_set ( 'implicit_flush', 1 );
	for($i = 0; $i < ob_get_level (); $i ++) {
		ob_end_flush ();
	}
	ob_implicit_flush ( 1 );
	echo "<!-- " . str_repeat ( ' ', 2000 ) . " -->";
}
?>
</body>
</html>