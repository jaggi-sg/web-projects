<!--
Name: JAGADISH SHIVANNA
Student ID: 1001050680
URL to run aplication: http://omega.uta.edu/~jxs0680/project3/buy.php
-->
<html>
<head>
<?php if (isset($_SESSION)) {
  session_start();
} 
?>
<title>Buy Products</title>
</head>
<body style="background-color: #A9A9A9; font-family: Trebuchet MS; font-size: 18">
<h1 style="color: cyan"><center>Shopping Cart Using Ebay API</center></h1>
<p>Shopping Basket:</p>

<table border='1'>
<?php
if(isset($_GET['clear'])) {
session_unset(); 
session_destroy(); 
}

if(isset($_GET['buy'])) 
array_push($_SESSION['id'], $_GET['buy']);

if(isset($_SESSION['sessions'])) {
$sessions = array();
$_SESSION['price'] = 0;
$sessions = $_SESSION['sessions'];
foreach($_SESSION['id'] as $key) {
$itemid = $sessions[intval($key)];
$items = explode(",", $itemid); ?>
<tr>
<td><img src="<?= $items[1] ?>"/></td>
<td><?= $items[0] ?></td>
<td><?= $items[2] ?></td>
</tr>
<?php $_SESSION['price'] = $_SESSION['price'] + $items[2]; ?>
<?php
} 
} ?>
</table>

<p>Total: <?php $_SESSION['price'] ?></p>
<form action="buy.php" method="GET">
<input type="hidden" name="clear" value="1"/>
<input type="submit" value="Empty Basket"/>
</form>

<?php $xmlstrcategories = file_get_contents('http://sandbox.api.ebaycommercenetwork.com/publisher/3.0/rest/CategoryTree?apiKey=78b0db8a-0ee1-4939-a2f9-d3cd95ec0fcc&visitorUserAgent&visitorIPAddress&trackingId=7000610&categoryId=72&showAllDescendants=true');
$xmlcategory = new SimpleXMLElement($xmlstrcategories); ?>
<form action="buy.php" method="GET">
<fieldset style="background-color: orange"><legend style="background-color: #E6E6FA">Find products:</legend>
<label>Category: 
<select name="category">
<?php foreach( $xmlcategory->category->categories->category as $child) { ?>
<option value="<?= $child['id'] ?>"> <?= $child->name ?></option>
<optgroup label="<?= $child->name ?>">
<?php foreach ( $child->categories->category as $child2) { ?>
<option value="<?= $child2['id'] ?>"> <?= $child2->name ?></option>
<?php } ?>
</optgroup>
<?php } ?>

<?php 
$searchValue = $_GET['search'];
$selectValue = $_GET['category']; 
$search = urlencode($searchValue); ?>
</select>
</label>
<label>Search keywords: <input type="text" name="search"/><label>
<input type="submit" value="Search"/>
</fieldset>
</form>

<?php
//print $search;
//print $selectValue;
if(!empty($searchValue)) {
$xmlstrsearch = file_get_contents("http://sandbox.api.ebaycommercenetwork.com/publisher/3.0/rest/GeneralSearch?apiKey=78b0db8a-0ee1-4939-a2f9-d3cd95ec0fcc&trackingId=7000610&categoryId=".$selectValue."&keyword=".$search."&numItems=20");
$xmlsearch = new SimpleXMLElement($xmlstrsearch);
?>

<p>
<table border="1" style="color: #4B0082; background-color: #DEB887">

<?php foreach( $xmlsearch->categories->category->items->product as $child2) { ?>
<tr> 
<?php
$name = strval($child2->name);
$image = strval($child2->images->image->sourceURL);
$price = strval($child2->minPrice);
$id = strval($child2['id']);
$sessions[$id] = array($name, $image, $price);
$result = implode(",", $sessions);
$_SESSION['sessions'] = $result;
$_SESSION['id'] = $id;
//print_r($_SESSION['sessions']); 
//print "<br>";
?>
<td><a href="buy.php?buy=<?= $child2['id']?>"><img src="<?= $child2->images->image->sourceURL ?>"/></a></td>
<td><?= $child2->name ?></td>
<td><?= $child2->minPrice ?></td>
<td><?= $child2->fullDescription ?></td>
</tr>
<?php
} 
}
?>
</table>

</p>
</body>
</html>