<!-- 
Name: JAGADISH SHIVANNA
Student ID: 1001050680
URL: http://omega.uta.edu/~jxs0680/project4/board.php
-->

<?php
session_start ();
?>

<html>
<head>
<title>Message Board</title>
</head>

<style>
#login {
	background-color: #745656;
	-webkit-border-radius: 5px;
	color: #fff;
	font-family: 'Palatino Linotype';
	font-size: 16px;
	border: 1px solid #745656;
}
</style>
<body
	style="background-color: #A9A9A9; font-family: Trebuchet MS; font-size: 16">

	<center>
		<h1 style="font-family: Harrington">Message Board using PHP</h1>
	</center>
	<hr />
	<form action="board.php" method="POST">
		<table>
			<tr>
				<td><label>User Name: </label></td>
				<td><input type="text" name="username"
					style="height: 25px; border: solid 1px #42AED8;" /></td>
			</tr>
			<tr>
				<td><br /></td>
			</tr>
			<tr>
				<td><label>Password: </label></td>
				<td><input type="password" name="password"
					style="height: 25px; border: solid 1px #42AED8;" /></td>
			</tr>
		</table>
		<p>
			<input type="submit" value="Login" name="login" id="login" />
		</p>
	</form>
	<p>New User? Register Here:</p>
	<form action="register.php" method="POST">
		<input type="submit" value="Register HERE" id="login" />
	</form>
</body>

</html>

<?php

error_reporting ( E_ALL );
ini_set ( 'display_errors', 'On' );

try {
	
	$dbname = dirname ( $_SERVER ["SCRIPT_FILENAME"] ) . "/mydb.sqlite";
	$dbh = new PDO ( "sqlite:$dbname" );
	$dbh->beginTransaction ();
	
	if (isset ( $_POST ['login'] )) {
		if (! empty ( $_POST ['username'] ) && ! empty ( $_POST ['password'] )) {
			$username = $_POST ['username'];
			$password = $_POST ['password'];
			$pass = md5 ( $password );
			$stmt = $dbh->prepare ( "select * from users where username=? and password=?" );
			$stmt->bindValue ( 1, $username );
			$stmt->bindValue ( 2, $pass );
			$stmt->execute ();
			
			if ($row = $stmt->fetch ()) {
				// echo "Full username";
				// $fullname = $row [2];
				$_SESSION ['username'] = $username;
				header ( "Location: http://omega.uta.edu/~jxs0680/project4/post.php" );
				// $_SESSION['fullname'] = $fullname;
			} else {
				echo "Invalid Username/Password";
			}
		} else {
			echo "Enter Username/Password to login";
		}
	}
} catch ( PDOException $e ) {
	print "Error!: " . $e->getMessage () . "<br/>";
	die ();
}
?>