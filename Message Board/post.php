<!-- 
Name: JAGADISH SHIVANNA
Student ID: 1001050680
URL: http://omega.uta.edu/~jxs0680/project4/board.php
-->
<?php
session_start ();
if (! $_SESSION ['username']) {
	header ( "Location: http://omega.uta.edu/~jxs0680/project4/board.php" );
}
?>
<html>
<head>
<title>Posts of <?= $_SESSION['username'] ?> Message Board</title>
<style>
#btn {
	background-color: #F8F2F2;
	-webkit-border-radius: 5px;
	color: rgb(71, 71, 211);
	font-family: 'Palatino Linotype';
	font-size: 14px;
	border: yellow solid 1px;
	font-weight: bold;
}

.scrollable {
	width: 100%;
	height: 100%;
	max-height: 300px;
	overflow: auto;
}
</style>


<body
	style="background-color: #A9A9A9; font-family: Trebuchet MS; font-size: 16">
	<h1 style="font-family: Harrington">Message Board using PHP</h1>
	<hr />
	<h2 style="font-family: Palatino Linotype">Messages</h2>
	<form action="post.php" method="POST">
		<p style="float: right">
			<input type="submit" value="Log Out" name="logout" id="btn">
		</p>
		<button id="btn" formaction="newpost.php">New Message</button>
	</form>

<?php
try {
	$dbname = dirname ( $_SERVER ["SCRIPT_FILENAME"] ) . "/mydb.sqlite";
	$dbh = new PDO ( "sqlite:$dbname" );
	$dbh->beginTransaction ();
	?>
	
	<p>
		Welcome to Message board: <span style='font-weight: bold;'>@<?= $_SESSION ['username'] ?> </span>
	</p>
	
	<?php
	echo "<br>";
	if (isset ( $_POST ['logout'] )) {
		header ( "Location: http://omega.uta.edu/~jxs0680/project4/board.php" );
		session_unset ();
		unset ( $_SESSION ['username'] );
	}
	
	$stmtfetch = $dbh->prepare ( "select id, postedby, fullname, follows, datetime, message from posts, users where username=postedby group by id order by datetime" );
	$stmtfetch->execute ();
	?>

<?php
	if (isset ( $_POST ['msgid'] )) {
		$reply = $_POST ['msgid'];
		$key = key ( $reply );
//Session for reply
		$_SESSION ['reply'] = $key;
		print_r ( $reply );
// 		print "Session value=";
// 		print $_SESSION ['reply'];
// 		print "<br>";
		print_r ( $_SESSION, TRUE );
		header ( "Location: http://omega.uta.edu/~jxs0680/project4/newpost.php" );
	}
	?>

<table border='1' align='center' style="border: solid 1px rgb(226, 226, 16); margin: 0px auto;">

		<form action='post.php' method='POST'>
<?php
	while ( $row = $stmtfetch->fetch () ) {
		// print $msg = "ID:".$row[0] .", USERNAME:".$row[1].", FULLNAME:".$row[2].", follow up of Message".$row[3].", TIME:".$row[4]."<br>".$row[5];
		// print $names = "<tr><td>".$row[2]."</td><td>@".$row[1]."</td><td>&nbsp;&bull;&nbsp;".$row[4]."</td></tr>";
		?>
			<tr>
				<th style='text-align: left; min-width: 800px; max-width: 800px;'><?= $row[2] ?>&nbsp;&bull;&nbsp;
				<span style='color: rgb(232, 233, 227);'>@<?= $row[1] ?></span>&nbsp;&bull;&nbsp; <?= $row[4] ?></th>
				<th><?= $row[0] ?></th>
			</tr>
			<tr>
				<td style='font-family: Arial; font-size: medium; min-width: 800px; max-width: 800px;'>
				<div class="scrollable"><?= $row[5] ?><br /> <br />
				<?php 
				if($row[3]!='') {
				?>
				<span style='font-style: italic; font-size: smaller;'>Follow up of Message:&nbsp;<?= $row[3] ?></span> <?php } ?>
					</div></td>
				<td style='text-align: center;'><input type='submit'
					name='msgid[<?= $row[0] ?>]' value='Reply' id='btn'></td>
			</tr>
<?php
		// print "<br>";
		// print "<input type='submit' name='msgid[$row[0]]' value='Reply'>";
		// echo "reply for msg ".$row[0];
		// $id=$row[0];
		// $sess[$id] = $row[0];
		// print_r($sess);
		// print $row[0];
		// print "<br>";
	}
	?>
</form>
	</table>

<?php
} 

catch ( PDOException $e ) {
	print "Error!: " . $e->getMessage () . "<br/>";
	die ();
}
?>
</body>
</html>

