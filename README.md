# Web Projects #

This repository contains a list of web projects done developed using PHP, Javascript, AJAX, HTML, and API's of dropbox, zillow, maps, lastfm, ebay.
### Dropbox Photo Album ###

* Upload image to dropbox and create photo album
* Delete image from album
* PHP, Dropbox API
* [Dropbox-photo album](http://omega.uta.edu/~jxs0680/photo-dropbox/album.php)

### Ebay Shopping Cart ###

* Shopping Cart
* Search for products
* eBay Commerce Network API (ECN API), PHP
* [ebay-cart](http://omega.uta.edu/~jxs0680/cart-ebay/buy.php)

### Lastfm ###

* Search Music artists
* Fetch artists album and event information
* AJAX, Javascript, Lastfm API
* [lastfm-music](http://omega.uta.edu/~jxs0680/music-lastfm/music.html)

### Message Board ###

* Micro-blogging message board similar to Twitter
* Registered users can post and reply to message posts
* PHP, SQLite
* [Microblog-message board](http://omega.uta.edu/~jxs0680/msg-board/board.php)

### Zillow Google Maps ###

* Search for house location on Maps
* Display its zillow price
* AJAX, Javascript, Zillow API, Google Maps API
* [maps-zillow](http://omega.uta.edu/~jxs0680/maps-zillow/map.html)

### Developed By: ###
[Jagadish Shivanna](https://www.linkedin.com/in/jagadishshivanna)