//Put your zillow.com API key here

var zwsid = "X1-ZWz1b3lj7bfpcb_at3ig";


var request = new XMLHttpRequest();

var geocoder, map, marker, infowindow;
var gmarkers = new Array();
//Start Initialize
function initialize () {

geocoder = new google.maps.Geocoder();
var myLatlng = new google.maps.LatLng(32.75,-97.13);  
var mapOptions = {
          center: myLatlng,
          zoom: 16
        };
        map = new google.maps.Map(document.getElementById('map'),
            mapOptions);

marker = new google.maps.Marker ({
position: myLatlng,
map: map
});
marker.setMap(map);
gmarkers.push(marker);

//Reverse GeoCode on map click
google.maps.event.addListener(map, 'click', function(event){
var newlatlng = event.latLng;
var lat = newlatlng.lat();
var lng = newlatlng.lng();
var latlng = new google.maps.LatLng(lat, lng);
updateMarker(event.latlng);
      geocoder.geocode({'latLng': latlng}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
         if (results[0]) {
          map.setZoom(16);
          marker = new google.maps.Marker({
            position: latlng,
            map: map
          });
gmarkers.push(marker);
var street, route, locality, aal1, postal_code;

for (i=0;i<results[0].address_components.length;i++){
    for (j=0;j<results[0].address_components[i].types.length;j++){
       if(results[0].address_components[i].types[j]=="street_number")
          street = results[0].address_components[i].short_name;
	if(results[0].address_components[i].types[j]=="route")
          route = results[0].address_components[i].short_name;
	if(results[0].address_components[i].types[j]=="locality")
          locality = results[0].address_components[i].short_name;
	if(results[0].address_components[i].types[j]=="administrative_area_level_1")
          aal1 = results[0].address_components[i].short_name;
	if(results[0].address_components[i].types[j]=="postal_code")
          postal_code = results[0].address_components[i].short_name;
    }
}
streetNum = street+" "+route;
  	//infowindow.setContent(results[0].formatted_address);
        infowindow.open(map, marker);
	document.forms["addressForm"]["address"].value=street+" "+route+", "+locality+", "+aal1+" "+postal_code;
	document.getElementById("outAddress").innerHTML = street+" "+route+", "+locality+", "+aal1+" "+postal_code;

request.onreadystatechange = displayResult;
request.open("GET","proxy.php?zws-id="+zwsid+"&address="+streetNum+"&citystatezip="+locality+"+"+aal1+"+"+postal_code);
request.withCredentials = "true";

request.send(null);

       	}
       } 
	else {
        alert("Geocoder failed due to: " + status);
       }
      });
});
//Reverse GeoCode End

google.maps.event.addListener(marker, 'click', function() {
infowindow = new google.maps.InfoWindow();   
infowindow.open(map, marker);
infowindow.setContent(formatted_address);
map.setZoom(16);
  });

infowindow = new google.maps.InfoWindow();
//infowindow.setContent(formatted_address);
infowindow.open(map, marker);

function updateMarker( location ) {
    marker = new google.maps.Marker({
        position: location,
        map: map
    });
    map.setCenter(location);
gmarkers.push(marker);
  }

}
google.maps.event.addDomListener(window, 'load', initialize);
//End Initialize

//Delete Markers on click of clear
function removeMarkers(){
    for(i=0; i<gmarkers.length; i++){
        gmarkers[i].setMap(null);
    }
gmarkers = [];
document.getElementById("address").value='';
}

//GeoCoding of map on input of address
function geoCodeAddress() {
geocoder = new google.maps.Geocoder();
    var address = document.getElementById("address").value;
    geocoder.geocode( { 'address': address }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
	map.setZoom(16);
        var marker = new google.maps.Marker({
            map: map,
	    position: results[0].geometry.location
        });
	//infowindow = new google.maps.InfoWindow();

	//infowindow.setContent(results[0].formatted_address);
	document.getElementById("outAddress").innerHTML = results[0].formatted_address;
        infowindow.open(map, marker);
	gmarkers.push(marker);
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
  }
//End GeoCodeAddress

function xml_to_string ( xml_node ) {
  
if (xml_node.xml)

return xml_node.xml;

var xml_serializer = new XMLSerializer();

return xml_serializer.serializeToString(xml_node);

}



function displayResult () {

if (request.readyState == 4) {

var xml = request.responseXML.documentElement;

var value = xml.getElementsByTagName("zestimate")[0].getElementsByTagName("amount")[0];
out = xml_to_string(value);
infowindow.setContent(document.getElementById("address").value+'\nPRICE='+out);
document.getElementById("output").innerHTML = out;
}

}



function sendRequest () {
request.onreadystatechange = displayResult;
var mapper = geoCodeAddress();
var content = document.getElementById("address").value;
var splitCon = content.split(', ');
var address = splitCon[0];
var city = splitCon[1];
var stateCon = splitCon[2];
var stateSplit = stateCon.split(' ');
var state = stateSplit[0];
var zipcode = stateSplit[1];

request.open("GET","proxy.php?zws-id="+zwsid+"&address="+address+"&citystatezip="+city+"+"+state+"+"+zipcode);
request.withCredentials = "true";

request.send(null);

}
